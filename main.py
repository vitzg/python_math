"""
Math module
"""


def add(*args):
    return int(sum(*args))


def add2(a, b, c):
    return a + b + c


def substract(a, b):
    return a - b


def multiply(a, b):
    return a * b


def divide(a, b):
    return a / b


def pow(a, b):
    return pow(a, b)


def sqrt(a, b):
    return sqrt(a, b)


def main(*args):
    result = add(args)
    return result


if __name__ == "__main__":
    pass
